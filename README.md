Usage
=====

To use in your project put `spank-extio/include` in the include path
and select the communication implementation:
- `dummy.c`: do nothing
- `i2c-bitters.c`: use I2C interface (requires the [bitters](https://gitlab.inria.fr/dalu/bitters) library)
- `libusb.c`: use USB interface (requires the libusb library)


| Compilation knob            | Description               |
|-----------------------------|---------------------------|
| `SPANK_EXTIO_WAIT_SPINNING` | If the IRQ line has not been wired to the hardware or is not available on the platform, it is necessary to perform spin wait in some part of the code. This could also be used with USB to perform polling instead of using interrupt transfer. <br> The wait time value is expressed in micro-seconds, a good default value is `5000`. |
| `SPANK_EXTIO_WITH_LOG`      | Enable tracing library behaviour (for debugging purpose). |
| `SPANK_EXTIO_WITH_ASSERT`   | Enable assertions.        |
| `SPANK_EXTIO_PLATFORM`      | Select the targeted platform. <br> Possible values are: `SPANK_EXTIO_PLATFORM_RPI`, `SPANK_EXTIO_PLATFORM_PX4VISION`. |
| `SPANK_EXTIO_I2C`           | Select the I2C device.    |
| `SPANK_EXTIO_IRQ`           | Select the IRQ GPIO line. |
| `SPANK_EXTIO_LIBUSB_DEBUG`  | Enable debug log for the libusb library |


| Function prefix      | Type | Requirement                    |
|----------------------|------|--------------------------------|
| `spank_extio_get_`   | Get  |                                |
| `spank_extio_set_`   | Set  |                                |
| `spank_extio_fetch_` | Get  | Data-ready (otherwise garbage) |

Caveat 
======

Some hardware/driver are not able to support more that one read
message per I2C transfer, if it is the case the `info` parameter
for the `spank_extio_fetch_distance_measurement()` **must** be set
to `NULL` otherwise function call will fail with `EOPNOTSUPP`. 
That the case for the RaspberryPI [Linux BCM2835 driver](https://elixir.bootlin.com/linux/v5.7.8/source/drivers/i2c/busses/i2c-bcm2835.c#L350).

Also on the RaspberryPI (again), there is a **bug** in the `BCM2835` hardware impacting the [I2C clock-stretching](https://elinux.org/BCM2835_datasheet_errata#p35_I2C_clock_stretching), this can result in failed I2C transfer, though the [zephr-redskin firmware](https://gitlab.inria.fr/dalu/zephyr-redskin/) will try it's best to recover from it and avoid bus outage.


Examples
========

LibUSB
------

~~~sh
cc ...                                                    \ 
   -I ${repo}/spank-extio/include                         \
   ${repo}/spank-extio/src/libusb.c                       \
   -lusb                                                  \
   ...
~~~

Raspberry PI
------------

~~~sh
cc ...                                                    \ 
   -DSPANK_EXTIO_PLATFORM=SPANK_EXTIO_PLATFORM_RPI        \
   -I ${repo}/spank-extio/include                         \
   ${repo}/spank-extio/src/i2c-bitters.c                  \
   ...
~~~

PX4 Vision
----------

As the platform doesn't expose GPIO line, it's not possible to use IRQ,
we need to fallback to spin-wait.

~~~sh
cc ...                                                    \ 
   -DSPANK_EXTIO_PLATFORM=SPANK_EXTIO_PLATFORM_PX4VISION  \
   -DSPANK_EXTIO_WAIT_SPINNING=5000                       \
   -I ${repo}/spank-extio/include                         \
   ${repo}/spank-extio/src/i2c-bitters.c                  \
   ...
~~~

I2C
---

If the `SPANK_EXTIO_PLATFORM` is not available for your platform,
you can directly specified the I2C device (required) and the IRQ
GPIO line (optional). Below an example for the Raspberry PI using `I2C_1`
and GPIO `P1.15`:

~~~sh
cc ...                                                               \ 
   -DSPANK_EXTIO_I2C='BITTERS_I2C_INITIALIZER(1)'                    \ 
   -DSPANK_EXTIO_IRQ='BITTERS_GPIO_PIN_INITIALIZER("gpiochip0", 22)' \
   -I ${repo}/spank-extio/include                                    \
   ${repo}/spank-extio/src/i2c-bitters.c                             \
   ...
~~~
