# CMake
#
# Copyright (c) 2019-2021
# Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
#
# SPDX-License-Identifier: Apache-2.0
#

#
# It is only mean to provide definitions for types and constants:
#   extio/types.h  : structures pass through the I2C/USB interfaces
#   extio/i2c.h    : I2C registers and values
#   extio/usb.h    : USB requests and values
#

zephyr_include_directories(../spank-extio/include)

