/*
 * Copyright (c) 2021-2022
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __SPANK__EXTIO__USB_H__
#define __SPANK__EXTIO__USB_H__

#include <spank/extio/constants.h>

// USB Vendor ID / Product ID
#define SPANK_USB_VENDOR_ID      				0x1915
#define SPANK_USB_PRODUCT_ID     				0x1807

// USB End Points
#define SPANK_USB_INT_EP_DATAREADY				1
#define SPANK_USB_INT_EP_EVENTS					2
#define SPANK_USB_BULK_EP_PACKETS				3

// [RW] Config Behaviour (Not Yet Implemented)
#define SPANK_USB_REQ_CONFIG_BEHAVIOUR				0x02
#define SPANK_USB_VAL_CONFIG_BEHAVIOUR_DROP_DEFAULT	   	\
    SPANK_EXTIO_BEHAVIOUR_DROP_DEFAULT
#define SPANK_USB_VAL_CONFIG_BEHAVIOUR_DROP_OLDEST	   	\
    SPANK_EXTIO_BEHAVIOUR_DROP_OLDEST
#define SPANK_USB_VAL_CONFIG_BEHAVIOUR_DROP_RECENT	   	\
    SPANK_EXTIO_BEHAVIOUR_DROP_RECENT
#define SPANK_USB_VAL_CONFIG_BEHAVIOUR_DROP_RANDOM		\
    SPANK_EXTIO_BEHAVIOUR_DROP_RANDOM

// [RW] Config Report
// Allows controlling of interrupt reporting for data-ready,
//  this won't affect the value returned by the data-ready request.
// Changing this value, only take effect on the next incoming data.
// Note: In the case of USB, the interrupt is not an IRQ line or a flag,
//       but the sending of the data using an "interrupt transfer".
//       The reported data will be prepend with a 32-bits (4-char) prefix:
//       the event id.
// Note  As a consequence of immediately sending the data,
//       the data-ready register will never be set for that event.
// Note: The SPANK_*_FLG_CONFIG_REPORT_DATA_READY is only available
//       for USB, if used it will only report data-ready value
//       to emulate an IRQ line
#define SPANK_USB_REQ_CONFIG_REPORT				0x03
#define SPANK_USB_VAL_CONFIG_REPORT_NONE			\
    SPANK_EXTIO_EVENT_NONE    
#define SPANK_USB_VAL_CONFIG_REPORT_ALL				\
    SPANK_EXTIO_EVENT_ALL
#define SPANK_USB_FLG_CONFIG_REPORT_DISTANCE_MEASUREMENT	\
    SPANK_EXTIO_EVENT_DISTANCE_MEASUREMENT
#define SPANK_USB_FLG_CONFIG_REPORT_CONTROL			\
    SPANK_EXTIO_EVENT_CONTROL
#define SPANK_USB_FLG_CONFIG_REPORT_DATA_READY			0x80

// [RO] Data Ready
// Retrieve list of data available.
// Note: trying to retrieve an unavailable data will return an error       
#define SPANK_USB_REQ_DATA_READY				0x05
#define SPANK_USB_FLG_DATA_READY_DISTANCE_MEASUREMENT		\
    SPANK_EXTIO_EVENT_DISTANCE_MEASUREMENT
#define SPANK_USB_FLG_DATA_READY_CONTROL			\
    SPANK_EXTIO_EVENT_CONTROL

// [RW] Posture
#define SPANK_USB_REQ_POSTURE					0x40

// [RO] Distance measurement
// If data is unavailable, it will be 0xff-filled
#define SPANK_USB_REQ_DISTANCE_MEASUREMENT			0x80
#define SPANK_USB_OPT_DISTANCE_MEASUREMENT_INFO			0x01
#define SPANK_USB_OPT_DISTANCE_MEASUREMENT_EXTRA		0x02

// [RW] Frame sniffer
#define SPANK_USB_REQ_FRAME_SNIFFER				0xe1
#define SPANK_USB_VAL_FRAME_SNIFFER_DISABLED			0x00
#define SPANK_USB_VAL_FRAME_SNIFFER_ENABLED			0x01

// [RO] Application Version
#define SPANK_USB_REQ_VERSION					0xf1

// [RO] Spank Version
#define SPANK_USB_REQ_SPANK_VERSION				0xf2


#endif
