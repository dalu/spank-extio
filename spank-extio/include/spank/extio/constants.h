/*
 * Copyright (c) 2020
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */


#ifndef __SPANK__EXTIO__CONSTANTS_H__
#define __SPANK__EXTIO__CONSTANTS_H__

/**
 * Default behaviour (if queue is full)
 */
#define SPANK_EXTIO_BEHAVIOUR_DROP_DEFAULT	0
/**
 * Drop oldest data (if queue is full)
 */
#define SPANK_EXTIO_BEHAVIOUR_DROP_OLDEST	1
/**
 * Drop most recent data (if queue is full)
 */
#define SPANK_EXTIO_BEHAVIOUR_DROP_RECENT	2
/**
 * Drop random data (if queue is full)
 */
#define SPANK_EXTIO_BEHAVIOUR_DROP_RANDOM	3


/**
 * Distance event
 */
#define SPANK_EXTIO_EVENT_DISTANCE_MEASUREMENT		0x01
/**
 * Control
 */
#define SPANK_EXTIO_EVENT_CONTROL			0x02
/**
 * All events
 */
#define SPANK_EXTIO_EVENT_ALL 				0x7f
/**
 * No events
 */
#define SPANK_EXTIO_EVENT_NONE 				0x00

#endif
