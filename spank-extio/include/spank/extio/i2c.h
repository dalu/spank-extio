/*
 * Copyright (c) 2020
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __SPANK__EXTIO__I2C_H__
#define __SPANK__EXTIO__I2C_H__

#include <spank/extio/constants.h>

// I2C Address
#define SPANK_I2C_ADDR_0					0x66

// [RW] Config Behaviour (Not Yet Implemented)
#define SPANK_I2C_REG_CONFIG_BEHAVIOUR				0x02
#define SPANK_I2C_VAL_CONFIG_BEHAVIOUR_DROP_DEFAULT	   	\
    SPANK_EXTIO_BEHAVIOUR_DROP_DEFAULT
#define SPANK_I2C_VAL_CONFIG_BEHAVIOUR_DROP_OLDEST	   	\
    SPANK_EXTIO_BEHAVIOUR_DROP_OLDEST
#define SPANK_I2C_VAL_CONFIG_BEHAVIOUR_DROP_RECENT	   	\
    SPANK_EXTIO_BEHAVIOUR_DROP_RECENT
#define SPANK_I2C_VAL_CONFIG_BEHAVIOUR_DROP_RANDOM		\
    SPANK_EXTIO_BEHAVIOUR_DROP_RANDOM

// [RW] Config Report
// Allows controlling of interrupt reporting for data-ready,
//  this won't affect the value returned by the data-ready register.
// Changing this value, only take effect on the next incoming data.
#define SPANK_I2C_REG_CONFIG_REPORT				0x03
#define SPANK_I2C_VAL_CONFIG_REPORT_NONE			\
    SPANK_EXTIO_EVENT_NONE    
#define SPANK_I2C_VAL_CONFIG_REPORT_ALL				\
    SPANK_EXTIO_EVENT_ALL
#define SPANK_I2C_FLG_CONFIG_REPORT_DISTANCE_MEASUREMENT	\
    SPANK_EXTIO_EVENT_DISTANCE_MEASUREMENT
#define SPANK_I2C_FLG_CONFIG_REPORT_CONTROL			\
    SPANK_EXTIO_EVENT_CONTROL

// [RO] Data Ready
// Retrieve list of data available.
// Note: trying to retrieve an unavailable data will return garbage
//       (though generally a 0xff-filled or zero-filed data)
// Note: Reading this register will de-assert the data-ready IRQ line
#define SPANK_I2C_REG_DATA_READY				0x05
#define SPANK_I2C_FLG_DATA_READY_DISTANCE_MEASUREMENT		\
    SPANK_EXTIO_EVENT_DISTANCE_MEASUREMENT
#define SPANK_I2C_FLG_DATA_READY_CONTROL			\
    SPANK_EXTIO_EVENT_CONTROL

// [RW] Posture
#define SPANK_I2C_REG_POSTURE					0x40

// [RO] Distance measurement
// If data is unavailable, it will be 0xff-filled
#define SPANK_I2C_REG_DISTANCE_MEASUREMENT			0x80

// [RO] Application Version
#define SPANK_I2C_REG_VERSION					0xf1

// [RO] Spank Version
#define SPANK_I2C_REG_SPANK_VERSION				0xf2


#endif
