/*
 * Copyright (c) 2020-2022
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */


#ifndef __SPANK__EXTIO__TYPES_H__
#define __SPANK__EXTIO__TYPES_H__

#include <spank/extio/basic-types.h>

/**
 */
typedef struct spank_extio_system_info {
    struct {
	char application[32]; 
	char spank[32];
    } version;
} spank_extio_system_info_t;


/**
 */
typedef struct spank_extio_info {
    uint16_t           pending;
    uint32_t	       lost;
} __attribute__((packed)) spank_extio_info_t;


/**
 */
typedef struct spank_extio_distance_measurement {
    spank_extio_distance_t distance;
    spank_extio_stddev_t   stddev;
    spank_extio_posture_t  posture;
    spank_extio_addr_t     id;
    uint32_t               freshness;
} __attribute__((packed)) spank_extio_distance_measurement_t;

typedef struct spank_extio_distance_measurement_extra {
    struct {
	spank_extio_rssi_t firstpath;
	spank_extio_rssi_t signal;
    }  __attribute__((packed)) power;
    struct {
	int32_t offset;
	uint32_t interval;
    }  __attribute__((packed)) clock_tracking;
    struct {
	uint16_t temp;
	uint16_t vbat;
    }  __attribute__((packed)) chip_info;
}  __attribute__((packed)) spank_extio_distance_measurement_extra_t;

typedef struct spank_extio_dumped_frame_header {
    uint16_t captured_length;
    uint16_t frame_length;
    uint64_t timestamp;
    uint64_t os_ticks;
    struct {
	spank_extio_rssi_t firstpath;
	spank_extio_rssi_t signal;
    } __attribute__((packed)) power;
    struct {
	int32_t offset;
	uint32_t interval;
    } __attribute__((packed)) clock_tracking;
} __attribute__((packed)) spank_extio_dumped_frame_header_t;


struct spank_extio_dumped_frame {
    spank_extio_dumped_frame_header_t header;
    uint8_t data[0];
}  __attribute__((packed));

#define SPANK_EXTIO_DUMPED_FRAME_SIZE(dumped_frame) 			\
    (sizeof(struct spank_extio_dumped_frame) +				\
     (dumped_frame)->header.captured_length)

#define SPANK_EXTIO_DUMPED_FRAME_MAXSIZE(frame_maxsize)			\
    (sizeof(struct spank_extio_dumped_frame) + (frame_maxsize))

#endif
