/*
 * Copyright (c) 2020
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

/*======================================================================*
 * Basic ExtIO types                                                    *
 * /!\ It is a direct mapping with SPANK types, keep it that way!   /!\ *
 *======================================================================*/

#ifndef __SPANK__EXTIO__BASIC_TYPES_H__
#define __SPANK__EXTIO__BASIC_TYPES_H__

#include <stdint.h>
#include <sys/types.h>
#include <inttypes.h>
#include <stdbool.h>

/**
 * SPANK network address.
 * @note it's an 802.15.4 address fitting on 64 bits
 */
typedef uint64_t spank_extio_addr_t;


/**
 * Signed 32-bit integer (with user defined NaN value).
 */
typedef int32_t spank_extio_int32_t;
#define SPANK_EXTIO_PRIint32  PRIi32
#define SPANK_EXTIO_INT32_MAX (INT32_MAX-1)
#define SPANK_EXTIO_INT32_MIN (-SPANK_EXTIO_INT32_MAX)
#define SPANK_EXTIO_INT32_NAN INT32_MIN
#define SPANK_EXTIO_INT32_INF INT32_MAX
#define SPANK_EXTIO_INT32_IS_NAN(v) ((v) == SPANK_EXTIO_INT32_NAN)

static inline bool
spank_extio_int32_from_int(spank_extio_int32_t *v, int i)
{
    if      (i > SPANK_EXTIO_INT32_MAX) { *v = +SPANK_EXTIO_INT32_INF;
	                                  return false;			}
    else if (i < SPANK_EXTIO_INT32_MIN) { *v = -SPANK_EXTIO_INT32_INF;
	                                  return false; 		}
    else                                { *v = i;
	                                  return true; 			}
}


/**
 * Signed 16-bit integer (with user defined NaN value).
 */
typedef int16_t spank_extio_int16_t;
#define SPANK_EXTIO_PRIint16  PRIi16
#define SPANK_EXTIO_INT16_MAX (INT16_MAX-1)
#define SPANK_EXTIO_INT16_MIN (-SPANK_EXTIO_INT16_MAX)
#define SPANK_EXTIO_INT16_NAN INT16_MIN
#define SPANK_EXTIO_INT16_INF INT16_MAX
#define SPANK_EXTIO_INT16_IS_NAN(v) ((v) == SPANK_EXTIO_INT16_NAN)

static inline bool
spank_extio_int16_from_int(spank_extio_int16_t *v, int i)
{
    if      (i > SPANK_EXTIO_INT16_MAX) { *v = +SPANK_EXTIO_INT16_INF;
	                                  return false; 		}
    else if (i < SPANK_EXTIO_INT16_MIN) { *v = -SPANK_EXTIO_INT16_INF;
	                                  return false; 		}
    else                                { *v = i;
	                                  return true;  		}
}


/**
 * Distance (signed int)
 * Expressed in centimeters.
 */
typedef spank_extio_int32_t spank_extio_distance_t;
#define SPANK_EXTIO_PRIdistance  SPANK_EXTIO_PRIint32
#define SPANK_EXTIO_DISTANCE_MAX SPANK_EXTIO_INT32_MAX
#define SPANK_EXTIO_DISTANCE_MIN SPANK_EXTIO_INT32_MIN
#define SPANK_EXTIO_DISTANCE_NAN SPANK_EXTIO_INT32_NAN
#define SPANK_EXTIO_DISTANCE_INF SPANK_EXTIO_INT32_INF
#define SPANK_EXTIO_DISTANCE_IS_NAN(v) ((v) == SPANK_EXTIO_DISTANCE_NAN)

static inline bool
spank_extio_distance_from_int(spank_extio_distance_t *v, int i)
{
    return spank_extio_int32_from_int(v, i);
}


/**
 * RSSI (unsigned int)
 * Expressed in -0.01 dBm
 */
typedef spank_extio_int16_t spank_extio_rssi_t;
#define SPANK_EXTIO_PRIrssi      SPANK_EXTIO_PRIint16
#define SPANK_EXTIO_RSSI_MAX     SPANK_EXTIO_INT16_MAX
#define SPANK_EXTIO_RSSI_MIN     SPANK_EXTIO_INT16_MIN
#define SPANK_EXTIO_RSSI_NAN     SPANK_EXTIO_INT16_NAN
#define SPANK_EXTIO_RSSI_INF     SPANK_EXTIO_INT16_INF
#define SPANK_EXTIO_RSSI_IS_NAN(v) ((v) == SPANK_EXTIO_RSSI_NAN)

static inline bool
spank_extio_rssi_from_int(spank_extio_rssi_t *v, int i)
{
    return spank_extio_int16_from_int(v, i);
}


/**
 * Stddev (signed int)
 * Expressed in 1/100.
 */
typedef spank_extio_int16_t spank_extio_stddev_t;
#define SPANK_EXTIO_PRIstddev  SPANK_EXTIO_PRIint16
#define SPANK_EXTIO_STDDEV_MAX SPANK_EXTIO_INT16_MAX
#define SPANK_EXTIO_STDDEV_MIN SPANK_EXTIO_INT16_MIN
#define SPANK_EXTIO_STDDEV_NAN SPANK_EXTIO_INT16_NAN
#define SPANK_EXTIO_STDDEV_INF SPANK_EXTIO_INT16_INF
#define SPANK_EXTIO_STDDEV_IS_NAN(v) ((v) == SPANK_EXTIO_STDDEV_NAN)

static inline bool
spank_extio_stddev_from_int(spank_extio_stddev_t *v, int i)
{
    return spank_extio_int16_from_int(v, i);
}


/**
 * Position (signed int)
 * Fields are expressed in centimeters.
 */
typedef union spank_extio_position {
    struct {
	spank_extio_int16_t x;
	spank_extio_int16_t y;
	spank_extio_int16_t z;
    };
    spank_extio_int16_t axis[3];
} spank_extio_position_t;
#define SPANK_EXTIO_PRIposition_axis  SPANK_EXTIO_PRIint16
#define SPANK_EXTIO_POSITION_AXIS_MAX SPANK_EXTIO_INT16_MAX
#define SPANK_EXTIO_POSITION_AXIS_MIN SPANK_EXTIO_INT16_MIN
#define SPANK_EXTIO_POSITION_AXIS_NAN SPANK_EXTIO_INT16_NAN
#define SPANK_EXTIO_POSITION_AXIS_INF SPANK_EXTIO_INT16_INF
#define SPANK_EXTIO_POSITION_NAN		\
    { .x = SPANK_EXTIO_INT16_NAN,		\
      .y = SPANK_EXTIO_INT16_NAN,		\
      .z = SPANK_EXTIO_INT16_NAN }
#define SPANK_EXTIO_POSITION_IS_NAN(v)		\
    (((v).x == SPANK_EXTIO_INT16_NAN) ||	\
     ((v).y == SPANK_EXTIO_INT16_NAN) ||	\
     ((v).z == SPANK_EXTIO_INT16_NAN))

static inline void
spank_extio_position_set_nan(spank_extio_position_t *v)
{
    v->x = v->y = v->z = SPANK_EXTIO_POSITION_AXIS_NAN;
}
static inline void
spank_extio_position_set_zero(spank_extio_position_t *v)
{
    v->x = v->y = v->z = 0;
}
static inline bool
spank_extio_position_from_int(spank_extio_position_t *v, int x, int y , int z)
{
    return spank_extio_int16_from_int(&v->x, x) &
   	   spank_extio_int16_from_int(&v->y, y) &
	   spank_extio_int16_from_int(&v->z, z);
}


/**
 * Velocity (signed int)
 * Fields are expressed in centimeters/second.
 */
typedef union spank_extio_velocity {
    struct {
	spank_extio_int16_t x;
	spank_extio_int16_t y;
	spank_extio_int16_t z;
    };
    spank_extio_int16_t axis[3];
} spank_extio_velocity_t;
#define SPANK_EXTIO_PRIvelocity_axis  SPANK_EXTIO_PRIint16
#define SPANK_EXTIO_VELOCITY_AXIS_MAX SPANK_EXTIO_INT16_MAX
#define SPANK_EXTIO_VELOCITY_AXIS_MIN SPANK_EXTIO_INT16_MIN
#define SPANK_EXTIO_VELOCITY_AXIS_NAN SPANK_EXTIO_INT16_NAN
#define SPANK_EXTIO_VELOCITY_AXIS_INF SPANK_EXTIO_INT16_INF
#define SPANK_EXTIO_VELOCITY_NAN		\
    { .x = SPANK_EXTIO_INT16_NAN,		\
      .y = SPANK_EXTIO_INT16_NAN,		\
      .z = SPANK_EXTIO_INT16_NAN }
#define SPANK_EXTIO_VELOCITY_IS_NAN(v)		\
    (((v).x == SPANK_EXTIO_INT16_NAN) ||	\
     ((v).y == SPANK_EXTIO_INT16_NAN) ||	\
     ((v).z == SPANK_EXTIO_INT16_NAN))

static inline void
spank_extio_velocity_set_nan(spank_extio_velocity_t *v)
{
    v->x = v->y = v->z = SPANK_EXTIO_VELOCITY_AXIS_NAN;
}
static inline void
spank_extio_velocity_set_zero(spank_extio_velocity_t *v) {
    v->x = v->y = v->z = 0;
}
static inline bool
spank_extio_velocity_from_int(spank_extio_velocity_t *v, int x, int y , int z)
{
    return spank_extio_int16_from_int(&v->x, x) &
   	   spank_extio_int16_from_int(&v->y, y) &
	   spank_extio_int16_from_int(&v->z, z);
}


/**
 * Stddev on xyz (signed int)
 * Fields are expressed in 1/100.
 */
typedef union spank_extio_stddev_xyz {
    struct {
	spank_extio_int16_t x;
	spank_extio_int16_t y;
	spank_extio_int16_t z;
    };
    spank_extio_int16_t axis[3];
} spank_extio_stddev_xyz_t;
#define SPANK_EXTIO_PRIstddev_axis  SPANK_EXTIO_PRIint16
#define SPANK_EXTIO_STDDEV_AXIS_MAX SPANK_EXTIO_INT16_MAX
#define SPANK_EXTIO_STDDEV_AXIS_MIN SPANK_EXTIO_INT16_MIN
#define SPANK_EXTIO_STDDEV_AXIS_NAN SPANK_EXTIO_INT16_NAN
#define SPANK_EXTIO_STDDEV_AXIS_INF SPANK_EXTIO_INT16_INF
#define SPANK_EXTIO_STDDEV_XYZ_NAN		\
    { .x = SPANK_EXTIO_INT16_NAN,		\
      .y = SPANK_EXTIO_INT16_NAN,		\
      .z = SPANK_EXTIO_INT16_NAN }
#define SPANK_EXTIO_STDDEV_XYZ_IS_NAN(v)	\
    (((v).x == SPANK_EXTIO_INT16_NAN) ||	\
     ((v).y == SPANK_EXTIO_INT16_NAN) ||	\
     ((v).z == SPANK_EXTIO_INT16_NAN))

static inline void
spank_extio_stddev_xyz_set_nan(spank_extio_stddev_xyz_t *v)
{
    v->x = v->y = v->z = SPANK_EXTIO_STDDEV_AXIS_NAN;
}
static inline void
spank_extio_stddev_xyz_set_zero(spank_extio_stddev_xyz_t *v)
{
    v->x = v->y = v->z = 0;
}
static inline bool
spank_extio_stddev_xyz_from_int(spank_extio_stddev_xyz_t *v, int x, int y , int z)
{
    return spank_extio_int16_from_int(&v->x, x) &
   	   spank_extio_int16_from_int(&v->y, y) &
	   spank_extio_int16_from_int(&v->z, z);
}


/**
 * Posture (signed int)
 */
typedef struct spank_extio_posture {
    struct {
	spank_extio_position_t   value;
	spank_extio_stddev_xyz_t stddev;
    } position;
    struct {
	spank_extio_velocity_t   value;
	spank_extio_stddev_xyz_t stddev;
    } velocity;
} spank_extio_posture_t;

#endif
