/*
 * Copyright (c) 2020
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef _SPANK__EXTIO_H_
#define _SPANK__EXTIO_H_

#include <spank/extio/types.h>
#include <spank/extio/constants.h>


/**
 * Get library flavor (i2c, usb, ...)
 *
 * @return flavor string
 */
char *spank_extio_flavor(void);

    
/**
 * Initialize library.
 *
 * @return < 0 in case of error (-errno value)
 */
int spank_extio_init(void);


/**
 * Get system information.
 *
 * @param[out] sysinfo
 *
 * @return < 0 in case of error (-errno value)
 */
int spank_extio_get_system_info(spank_extio_system_info_t *sysinfo);


/**
 * Or-ed list of events we want to collect.
 *
 * @param events_msk    or-ed list of events (SPANK_EXTIO_EVENT_*)
 * @param behaviour	SPANK_EXTIO_BEHAVIOUR_*
 *
 * @return < 0 in case of error (-errno value)
 */
int spank_extio_start(int events_msk, int behaviour);


/**
 * Terminate the event collection process
 *
 * @return < 0 in case of error (-errno value)
 */
int spank_extio_end(void);


/**
 * Get or-ed list of events which are ready for retrieval.
 * @note The function spank_extio_wait() should be preferred as first run.
 * @note All available events are retrieve (not limited to the one listed
 *       in spank_extio_start())
 * @note Even if an IRQ line has been asserted it can return 
 *       SPANK_EXTIO_EVENT_NONE (due to simultaneous use of ExtIO flavors)
 * @note In I2C implementation, the IRQ line will be de-asserted,
 *       as a read of the I2C DATA_READY register is performed
 *
 * @return < 0 in case of error (-errno value)
 */
int spank_extio_events_ready(void);


/**
 * Wait for extio provided events (blocking operation).
 * This function will wait for events registered by spank_extio_start().
 * @note When the function return an event, it means that at least
 *       one data is available, all data available for that event
 *       must be read as no further event will be generated for them.
 *       That's the case for distance measurement which is able to
 *       store several data instances.
 * @note Due to the way interrupt is processed internally (delayed due to
 *       user space processing), the function can return -EAGAIN.
 *       In this case we have no events ready, but we lost information about
 *       timeout; if precise timeout handing is required it should be
 *       ensure using clock_gettime(CLOCK_MONOTONIC, ...)
 *
 * @param timeout		timeout in milliseconds
 *
 * @return -EAGAIN (some part of the timeout value can have been elapsed)
 * @return < 0 in case of error (-errno value)
 * @return 0 in case of timeout
 * @return or-ed list of ready events
 */
int spank_extio_wait(int timeout);


/**
 * Fetch distance measurement data.
 * @note Will successfully return with garbage data, if no data is ready
 *       (see spank_extio_events_ready() and spank_extio_wait()). 
 * @note Some hardware/driver are not able to support more that
 *       one read message per i2c transfer, if it is the case the `info`
 *       parameter must be set to NULL otherwise EOPNOTSUPP will be returned.
 *       (That the case for the RaspberryPI Linux BCM2835 driver).
 * @note In case of I2C the extra parameter should be set to NULL.
 *
 * @param[out] dm		distance measurement
 * @param[out] extra		extra information (can be NULL)
 * @param[out] info		information (can be NULL)
 *
 * @return < 0 in case of error (-errno value)
 */
int spank_extio_fetch_distance_measurement(
	spank_extio_distance_measurement_t *dm,
	spank_extio_distance_measurement_extra_t *extra,
	spank_extio_info_t *info);


/**
 * Set the current posture.
 *
 * @param posture
 * @return < 0 in case of error (-errno value)
 */
int spank_extio_set_posture(spank_extio_posture_t *posture);


/**
 * Get the current posture.
 *
 * @param posture
 * @return < 0 in case of error (-errno value)
 */
int spank_extio_get_posture(spank_extio_posture_t *posture);


/**
 * Enable frame sniffer.
 */
int spank_extio_enable_frame_sniffer(void);


/**
 * Disable frame sniffer.
 */
int spank_extio_disable_frame_sniffer(void);


/**
 * Get a sniffed frame.
 * 
 * @param [out]    buf     buffer 
 * @param [in,out] buflen  size of buffer / transferred data
 *
 * @return < 0 in case of error (-errno value)
 * @return transferred data size
 */
ssize_t spank_extio_frame_sniffer(uint8_t *buf, size_t buflen);

#endif
