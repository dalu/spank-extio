/*
 * Copyright (c) 2021
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __SPANK__EXTIO_I2C_BITTERS_PX4VISION_H__
#define __SPANK__EXTIO_I2C_BITTERS_PX4VISION_H__

/* On PX4Vision:
 *  - GPIO pin are not exposed, 
 *    so it's not possible to define IRQ for SPANK-ExtIO
 *  - available I2C is /dev/i2c-1
 */

#undef  SPANK_EXTIO_IRQ
#define SPANK_EXTIO_I2C		BITTERS_I2C_INITIALIZER(1)

#endif
