/*
 * Copyright (c) 2021-2022
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>

#if defined(__linux__) || defined(__APPLE__)
#include <libusb-1.0/libusb.h>
#else
#include <libusb.h>
#endif

#include <spank/extio.h>
#include <spank/extio/usb.h>

#include "debug-common.h"



/*======================================================================*/
/* Local variables                                                      */
/*======================================================================*/


#if defined(SPANK_EXTIO_WAIT_SPINNING)
static int spank_extio_events_msk = SPANK_EXTIO_EVENT_NONE;
#endif

static struct libusb_context       *spank_usb_ctx  = NULL;
static struct libusb_device_handle *spank_usb_devh = NULL;


/*======================================================================*/
/* Local functions                                                      */
/*======================================================================*/

static int
errno_map_libusb_error(int error)
{
    switch(error) {
    case LIBUSB_SUCCESS:             return 0;
    case LIBUSB_ERROR_IO:            return EIO;
    case LIBUSB_ERROR_INVALID_PARAM: return EINVAL;
    case LIBUSB_ERROR_ACCESS:        return EACCES;
    case LIBUSB_ERROR_NO_DEVICE:     return ENXIO;
    case LIBUSB_ERROR_NOT_FOUND:     return ENOENT;
    case LIBUSB_ERROR_BUSY:          return EBUSY;
    case LIBUSB_ERROR_TIMEOUT:       return ETIMEDOUT;
    case LIBUSB_ERROR_OVERFLOW:      return EOVERFLOW;
    case LIBUSB_ERROR_PIPE:          return EPIPE;
    case LIBUSB_ERROR_INTERRUPTED:   return EINTR;
    case LIBUSB_ERROR_NO_MEM:        return ENOMEM;
    case LIBUSB_ERROR_NOT_SUPPORTED: return EOPNOTSUPP;
    case LIBUSB_ERROR_OTHER:         return ENOMSG;
    default:                         return ENOMSG;
    }
}


static inline int
_spank_extio_usb_intr(void *data, size_t length, unsigned int timeout)
{
    int transferred;
    int rc = libusb_interrupt_transfer(
	       spank_usb_devh,
	       SPANK_USB_INT_EP_DATAREADY | LIBUSB_ENDPOINT_IN,
	       data, length, &transferred, timeout);
    return rc < 0 ? -errno_map_libusb_error(rc) : transferred;
}


static inline int
_spank_extio_usb_get_with_opt(int request, int opt, void *data, size_t length)
{
    uint8_t  bmRequestType = LIBUSB_ENDPOINT_IN         | // Device to host
	                     LIBUSB_REQUEST_TYPE_VENDOR | // Vendor
	                     LIBUSB_RECIPIENT_INTERFACE ; // Interface
    uint8_t  bRequest      = request;
    uint16_t wValue        = opt;
    uint16_t wIndex        = 0x0000;
    uint16_t wLength       = length;
    unsigned int timeout   = 0;

    int rc = libusb_control_transfer(spank_usb_devh,
				     bmRequestType, bRequest,
				     wValue, wIndex,
				     data, wLength, timeout);
    return rc < 0 ? -errno_map_libusb_error(rc) : rc;
}

static inline int
_spank_extio_usb_get(int request, void *data, size_t length)
{
    return _spank_extio_usb_get_with_opt(request, 0x0000, data, length);
}


static inline int
_spank_extio_usb_set(int request, void *data, size_t length)
{
    uint8_t  bmRequestType = LIBUSB_ENDPOINT_OUT        | // Host to device
	                     LIBUSB_REQUEST_TYPE_VENDOR | // Vendor
	                     LIBUSB_RECIPIENT_INTERFACE ; // Interface
    uint8_t  bRequest      = request;
    uint16_t wValue        = 0x0000;
    uint16_t wIndex        = 0x0000;
    uint16_t wLength       = length;
    unsigned int timeout   = 0;
    
    int rc = libusb_control_transfer(spank_usb_devh,
				     bmRequestType, bRequest,
				     wValue, wIndex,
				     data, wLength, timeout);
    return rc < 0 ? -errno_map_libusb_error(rc) : rc;
}


static inline int
spank_extio_set_report(int events_msk)
{
    uint8_t flags = events_msk | SPANK_USB_FLG_CONFIG_REPORT_DATA_READY;

#if defined(SPANK_EXTIO_WAIT_SPINNING)
    // In the case of USB, interrupt mode is not a line which is asserted
    // but an USB transfer of type interrupt. Which means enabling report
    // will *consume* the data and the DATA_READY will always be empty.
    // ==> Never really enable report when compiled for spin-wait
    spank_extio_events_msk = events_msk;
    flags                  = SPANK_EXTIO_EVENT_NONE;
#endif
    
    return _spank_extio_usb_set(SPANK_USB_REQ_CONFIG_REPORT,
				&flags, sizeof(flags));
}


static inline int
spank_extio_set_behaviour(int behaviour)
{
    uint8_t val = behaviour;
    return _spank_extio_usb_set(SPANK_USB_REQ_CONFIG_BEHAVIOUR,
				&val, sizeof(val));
}



/*======================================================================*/
/* Exported functions                                                   */
/*======================================================================*/

char *
spank_extio_flavor(void)
{
    return "USB (libusb)";
}


int
spank_extio_init(void)
{
    int rc = -EINVAL;
    
    /*
     * Initialize libusb
     */
    if ((rc = libusb_init(&spank_usb_ctx)) < 0) {
	SPANK_EXTIO_LOG_RC_LIBUSB(rc, "Error initializing libusb");
	return -errno_map_libusb_error(rc);
    }
    
#ifdef SPANK_EXTIO_LIBUSB_DEBUG
    /* Set debugging output to max level.
     */
    libusb_set_debug(spank_usb_ctx, SPANK_EXTIO_LIBUSB_DEBUG);
#endif

    /* Look for a specific device and open it.
     */
    SPANK_EXTIO_LOG("Looking up for device <0x%04x, 0x%04x>",
		    SPANK_USB_VENDOR_ID, SPANK_USB_PRODUCT_ID);
    spank_usb_devh = libusb_open_device_with_vid_pid(spank_usb_ctx,
						     SPANK_USB_VENDOR_ID,
						     SPANK_USB_PRODUCT_ID);
    if (! spank_usb_devh) {
	SPANK_EXTIO_LOG("Unable to find device <0x%04x, 0x%04x>",
			SPANK_USB_VENDOR_ID, SPANK_USB_PRODUCT_ID);
	rc = -ENODEV;
	goto failed;
    }

    /* Claim interface: 0 */
    if (libusb_kernel_driver_active(spank_usb_devh, 0)) {
	libusb_detach_kernel_driver(spank_usb_devh, 0);
    }
    if ((rc = libusb_claim_interface(spank_usb_devh, 0)) < 0) {
	SPANK_EXTIO_LOG_RC_LIBUSB(rc, "Error claiming interface");
	goto failed;
    }
    
    /* Done 
     */
    SPANK_EXTIO_LOG("Initialized");
    return 0;


    /* Failed
     */
failed:
    if (spank_usb_devh) {
	libusb_release_interface(spank_usb_devh, 0);
	libusb_close(spank_usb_devh);
    }
    if (spank_usb_ctx) {
	libusb_exit(spank_usb_ctx);
    }
    return rc;
}


int
spank_extio_get_system_info(spank_extio_system_info_t *sysinfo)
{
    int rc;

    if (((rc = _spank_extio_usb_get(SPANK_USB_REQ_VERSION,
                            sysinfo->version.application,
                            sizeof(sysinfo->version.application))) < 0) ||
	((rc = _spank_extio_usb_get(SPANK_USB_REQ_SPANK_VERSION,
                            sysinfo->version.spank,
                            sizeof(sysinfo->version.spank))) < 0)) {
       return rc;
    }

    SPANK_EXTIO_LOG("Retrieved system information (app=<%s>, spank=<%s>)",
                   sysinfo->version.application,
                   sysinfo->version.spank);
    return 0;
}


int
spank_extio_start(int events_msk, int behaviour)
{
    int rc;
    
    if (((rc = spank_extio_set_report(events_msk)  ) < 0) ||
	((rc = spank_extio_set_behaviour(behaviour)) < 0)) {
	SPANK_EXTIO_LOG_RC_ERRNO(rc, "Failed to start");
	return rc;
    }

    SPANK_EXTIO_LOG("Started with report=<0x%02x> and behaviour=<0x%02x>",
		    events_msk, behaviour);
    return 0;
}


int
spank_extio_end()
{
    int rc = spank_extio_set_report(SPANK_EXTIO_EVENT_NONE);
    if (rc < 0) {
	SPANK_EXTIO_LOG_RC_ERRNO(rc, "Failed to end");
    } else {
	SPANK_EXTIO_LOG("Ended");
    }
    return rc;
}


int
spank_extio_wait(int timeout)
{
#if defined(SPANK_EXTIO_WAIT_SPINNING)
#warning using SPANK_EXTIO_WAIT_SPINNING is non-optimal
    uint32_t utimeout = timeout * 1000;
    while(1) {
	// Retrieve events
	int events = spank_extio_events_ready();
	if (events < 0) {
	    SPANK_EXTIO_LOG_RC_ERRNO(events, "Failed to retrieve events");
	    return events;
	}
	SPANK_EXTIO_LOG("Got events <0x%02x>", events);

	// Return if we got an event for which we are interested
	if (events & spank_extio_events_msk) {
	    return events;
	}

	// Deal with timeout (otherwise endless loop)
	if        (timeout == 0) {
	    SPANK_EXTIO_LOG("Returned without events");
	    return SPANK_EXTIO_EVENT_NONE;
	} else if (timeout >  0) {
	    if (usleep(SPANK_EXTIO_WAIT_SPINNING) < 0) {
		SPANK_EXTIO_LOG_ERRNO("Spin wait failed");
		return -errno;
	    }
	    if (utimeout < SPANK_EXTIO_WAIT_SPINNING ) {
		SPANK_EXTIO_LOG("Returned from timeout");
		return SPANK_EXTIO_EVENT_NONE;
	    }
	    utimeout -= SPANK_EXTIO_WAIT_SPINNING;
	}
    }
#else
    uint8_t data_ready  = SPANK_EXTIO_EVENT_NONE;
    ssize_t transferred = _spank_extio_usb_intr(&data_ready, sizeof(data_ready),
						timeout);
    if (transferred < 0) {
	SPANK_EXTIO_LOG_RC_ERRNO(transferred, "Failed to retrieve events");
	return transferred;
    } else if (transferred != sizeof(data_ready)) {
	SPANK_EXTIO_LOG("Failed to retrieve events (truncated data)");
	return -EINVAL;
    }
    return data_ready;
#endif
}


int
spank_extio_events_ready(void)
{
    uint8_t flags = 0;
    int     rc    = _spank_extio_usb_get(SPANK_USB_REQ_DATA_READY,
					 &flags, sizeof(flags));
    return rc < 0 ? rc : flags;
}


int
spank_extio_fetch_distance_measurement(
	spank_extio_distance_measurement_t *dm,
	spank_extio_distance_measurement_extra_t *extra,
	spank_extio_info_t *info)
{
    // Build option set
    uint opt = 0x0000;
    if (extra != NULL) opt |= SPANK_USB_OPT_DISTANCE_MEASUREMENT_EXTRA;
    if (info  != NULL) opt |= SPANK_USB_OPT_DISTANCE_MEASUREMENT_INFO;

    // Allocated 0xff-filed memory on the stack
    char data[sizeof(spank_extio_distance_measurement_t      ) +
	      sizeof(spank_extio_distance_measurement_extra_t) +
	      sizeof(spank_extio_info_t                      )];
    memset(&data, 0xff, sizeof(data));
    
    // Perform USB request with option
    int rc = _spank_extio_usb_get_with_opt(SPANK_USB_REQ_DISTANCE_MEASUREMENT,
					   opt, &data, sizeof(data));

    // Split data
    if (rc >= 0) {
	int offset = 0;
	memcpy(dm, &data[offset], sizeof(spank_extio_distance_measurement_t));
	offset += sizeof(spank_extio_distance_measurement_t);

	if (extra != NULL) {
	    memcpy(extra, &data[offset],
		   sizeof(spank_extio_distance_measurement_extra_t));
	    offset += sizeof(spank_extio_distance_measurement_extra_t);
	}

	if (info != NULL) {
	    memcpy(info, &data[offset], sizeof(spank_extio_info_t));
	}
    }
    
    // Returns
    return rc;
}


int
spank_extio_set_posture(spank_extio_posture_t *posture)
{
    return _spank_extio_usb_set(SPANK_USB_REQ_POSTURE,
				posture, sizeof(*posture));
}


int
spank_extio_get_posture(spank_extio_posture_t *posture)
{
    return _spank_extio_usb_get(SPANK_USB_REQ_POSTURE,
				posture, sizeof(*posture));
}


int
spank_extio_enable_frame_sniffer(void)
{
    uint8_t action = SPANK_USB_VAL_FRAME_SNIFFER_ENABLED;
    return _spank_extio_usb_set(SPANK_USB_REQ_FRAME_SNIFFER,
				&action, sizeof(action));
}

int
spank_extio_disable_frame_sniffer(void)
{
    uint8_t action = SPANK_USB_VAL_FRAME_SNIFFER_DISABLED;
    return _spank_extio_usb_set(SPANK_USB_REQ_FRAME_SNIFFER,
				&action, sizeof(action));
}

ssize_t
spank_extio_frame_sniffer(uint8_t *buf, size_t buflen) {
    int transferred;

    // At least require the buffer to hold the whole dumped frame header
    if (buflen < sizeof(struct spank_extio_dumped_frame_header)) {
	return -EINVAL;
    }

    // Retrieve dumped frame
    int rc = libusb_bulk_transfer(spank_usb_devh, SPANK_USB_BULK_EP_PACKETS |
				                  LIBUSB_ENDPOINT_IN,
				  buf, buflen, &transferred, 0);
    if (rc < 0) {
	return -errno_map_libusb_error(rc);
    }

    // Sanity check
    if (transferred < sizeof(struct spank_extio_dumped_frame_header)) {
	return -EIO;
    }

    // Adjust captured_length field
    struct spank_extio_dumped_frame *dumped_frame = (void *)buf;
    if ((transferred - sizeof(struct spank_extio_dumped_frame_header)) <
	dumped_frame->header.captured_length) {
	dumped_frame->header.captured_length = transferred - sizeof(struct spank_extio_dumped_frame_header);
    }

    // Job's done
    return transferred;
}
