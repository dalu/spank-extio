/*
 * Copyright (c) 2020-2022
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <unistd.h>
#include <string.h>
#include <errno.h>

#include <spank/extio.h>

#include "debug-common.h"


static spank_extio_posture_t     spank_extio_dummy_posture     = { 0 };
static spank_extio_system_info_t spank_extio_dummy_system_info = {
       .version.application = "dummy",
       .version.spank       = "dummy",
};



char *
spank_extio_flavor(void)
{
    return "dummy";
}

int
spank_extio_init(void)
{
    SPANK_EXTIO_LOG("Initialized");
    return 0; // Ok
}


int
spank_extio_get_system_info(spank_extio_system_info_t *sysinfo)
{
    memcpy(sysinfo, &spank_extio_dummy_system_info,
          sizeof(spank_extio_system_info_t));
    return 0; // Ok
}


int
spank_extio_start(int events_msk, int behaviour)
{
    SPANK_EXTIO_LOG("Started with report=<0x%02x> and behaviour=<0x%02x>",
		    events_msk, behaviour);
    return 0; // Ok
}


int
spank_extio_end(void)
{
    SPANK_EXTIO_LOG("Ended");
    return 0; // Ok
}


int
spank_extio_events_ready(void)
{
    return 0; // No event ready
}


int
spank_extio_wait(int timeout)
{
    // If timeout, return when expired (as we don't have event)
    if        (timeout == 0) {
	return SPANK_EXTIO_EVENT_NONE;
    } else if (timeout > 0) {
	if (usleep(timeout * 1000) < 0) {
	    SPANK_EXTIO_LOG_ERRNO("Spin wait failed");
	    return -errno;
	}
	SPANK_EXTIO_LOG("Returned from timeout");
	return SPANK_EXTIO_EVENT_NONE;
    }

    // No event.... endless wait
    while(1) {
	sleep(60);
    }

    return -EAGAIN;
}


int
spank_extio_fetch_distance_measurement(
	spank_extio_distance_measurement_t *dm,
	spank_extio_info_t *info)
{
    SPANK_EXTIO_LOG("Fetching distance measurement");
    // Return garbage data (it's in the doc...)
    memset(dm, 0xff, sizeof(*dm));
    if (info) memset(info, 0xff, sizeof(*info));
    return 0;
}


int
spank_extio_set_posture(spank_extio_posture_t *posture)
{
    SPANK_EXTIO_LOG("Setting posture");
    memcpy(&spank_extio_dummy_posture, posture, sizeof(*posture));
    return 0;
}


int
spank_extio_get_posture(spank_extio_posture_t *posture)
{
    SPANK_EXTIO_LOG("Getting posture");
    memcpy(posture, &spank_extio_dummy_posture, sizeof(*posture));
    return 0;
}

int
spank_extio_enable_frame_sniffer(void)
{
    return -EOPNOTSUPP;
}

int
spank_extio_disable_frame_sniffer(void)
{
    return -EOPNOTSUPP;
}

ssize_t
spank_extio_frame_sniffer(uint8_t *buf, size_t buflen) {
    return -EOPNOTSUPP;
}
