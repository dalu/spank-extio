/*
 * Copyright (c) 2021
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __SPANK__EXTIO_PLATFORM_H__
#define __SPANK__EXTIO_PLATFORM_H__

#define SPANK_EXTIO_PLATFORM_UNKNOWN		0
#define SPANK_EXTIO_PLATFORM_RPI 		1
#define SPANK_EXTIO_PLATFORM_PX4VISION		2
#define SPANK_EXTIO_PLATFORM_LIBUSB		3

#endif
