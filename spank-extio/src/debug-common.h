/*
 * Copyright (c) 2020
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __SPANK__EXTIO_COMMON_H__
#define __SPANK__EXTIO_COMMON_H__

#ifdef SPANK_EXTIO_WITH_LOG
#ifndef SPANK_EXTIO_LOG
#include <stdio.h>
#include <errno.h>
#include <string.h>
#define SPANK_EXTIO_LOG(x, ...) do {					\
	int errno_saved = errno;					\
	fprintf(stderr, "SPANK ExtIO: " x "\n", ##__VA_ARGS__);		\
	errno = errno_saved;						\
    } while(0)
#endif
#else
#define SPANK_EXTIO_LOG(x, ...)
#endif

#define SPANK_EXTIO_LOG_ERRNO(x, ...)					\
    SPANK_EXTIO_LOG(x " (%s)", ##__VA_ARGS__, strerror(errno))

#define SPANK_EXTIO_LOG_RC_ERRNO(rc, x, ...)				\
    SPANK_EXTIO_LOG(x " (%s)", ##__VA_ARGS__, strerror(-rc))

#define SPANK_EXTIO_LOG_RC_LIBUSB(rc, x, ...)				\
    SPANK_EXTIO_LOG(x " (%s)", ##__VA_ARGS__, libusb_error_name(rc))


#ifdef SPANK_EXTIO_WITH_ASSERT
#ifndef SPANK_EXTIO_ASSERT
#include <assert.h>
#define SPANK_EXTIO_ASSERT(x)						\
    assert(x)
#endif
#else
#define SPANK_EXTIO_ASSERT(x)
#endif

#endif
