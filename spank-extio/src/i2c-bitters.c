/*
 * Copyright (c) 2020-2022
 * Stephane D'Alu, Inria Chroma / Inria Agora, INSA Lyon, CITI Lab.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

/* bitters_i2c_transfert() should be thread safe without locking
 * as it is locally rebuilding transfert message and use a single ioctl
 * (ioctl is thread safe)
 */

#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <poll.h>

#include <bitters.h>
#include <bitters/gpio.h>
#include <bitters/i2c.h>

#include <spank/extio.h>
#include <spank/extio/i2c.h>

#include "debug-common.h"


/* Import platform information, or ensure that i2c/irq information are provided
 * as SPANK_EXTIO_I2C (required) and SPANK_EXTIO_IRQ (optional)
 */
#include "platform.h"

#if !defined(SPANK_EXTIO_PLATFORM)
#  if !defined(SPANK_EXTIO_I2C)
#    error Platform not defined and i2c/irq information not provided, define either SPANK_EXTIO_PLATFORM or SPANK_EXTIO_I2C / SPANK_EXTIO_IRQ
#  endif
#elif SPANK_EXTIO_PLATFORM == SPANK_EXTIO_PLATFORM_RPI
#  include "i2c-bitters-rpi.h"
#elif SPANK_EXTIO_PLATFORM == SPANK_EXTIO_PLATFORM_PX4VISION
#  include "i2c-bitters-px4vision.h"
#else
#  error Platform not supported (see: SPANK_EXTIO_PLATFORM)
#endif


/*======================================================================*/
/* Macros                                                               */
/*======================================================================*/

#ifndef __arraycount
#define	__arraycount(__x)	(sizeof(__x) / sizeof(__x[0]))
#endif



/*======================================================================*/
/* Local variables                                                      */
/*======================================================================*/

/* GPIO/I2C for SPANK module
 */ 
#if defined(SPANK_EXTIO_IRQ)
static bitters_gpio_pin_t spank_irq      = SPANK_EXTIO_IRQ;
#endif
static bitters_i2c_t      spank_i2c      = SPANK_EXTIO_I2C;
static bitters_i2c_addr_t spank_i2c_addr = BITTERS_I2C_ADDR_8|SPANK_I2C_ADDR_0;

static struct bitters_i2c_cfg spank_i2c_cfg = {
#if defined(__linux__)
    .speed = 0	  // Linux doesn't allow changing speed.
                  // => Use boot-time selected speed.
                  //    Modify it with config.txt, modprobe, or device-tree
#else
#error Support for this operating system not yet implemented
#endif
};

#if defined(SPANK_EXTIO_WAIT_SPINNING)
static int spank_extio_events_msk = SPANK_EXTIO_EVENT_NONE;
#endif



/*======================================================================*/
/* Local functions                                                      */
/*======================================================================*/

static inline int
_spank_extio_i2c_read(uint8_t reg, void *data, size_t datalen)
{
    const struct bitters_i2c_transfert xfr[] = {
        { .buf = &reg, .len = sizeof(reg), .write = 1, },
	{ .buf = data, .len = datalen,     .read  = 1, },
    };

    int rc = bitters_i2c_transfert(&spank_i2c, spank_i2c_addr,
				   xfr, __arraycount(xfr));
    return rc < 0 ? rc : 0;
}


static inline int
_spank_extio_i2c_write(uint8_t reg, void *data, size_t datalen)
{
    const struct bitters_i2c_transfert xfr[] = {
        { .buf = &reg, .len = sizeof(reg), .write = 1, },
	{ .buf = data, .len = datalen,     .write = 1, },
    };

    int rc = bitters_i2c_transfert(&spank_i2c, spank_i2c_addr,
				  xfr, __arraycount(xfr));
    return rc < 0 ? rc : 0;
}


static inline int
spank_extio_set_report(int events_msk)
{
    uint8_t flags = events_msk;
    return _spank_extio_i2c_write(SPANK_I2C_REG_CONFIG_REPORT,
				  &flags, sizeof(flags));
}


static inline int
spank_extio_set_behaviour(int behaviour)
{
    uint8_t val = behaviour;
    return _spank_extio_i2c_write(SPANK_I2C_REG_CONFIG_BEHAVIOUR,
				  &val, sizeof(val));
}



/*======================================================================*/
/* Exported functions                                                   */
/*======================================================================*/

char *
spank_extio_flavor(void)
{
    return "i2c (bitters)";
}


int
spank_extio_init(void)
{
    int rc = -EINVAL;
    
    /*
     * Enable I2C and configure ExtIO
     */
    if ((rc = bitters_i2c_enable(&spank_i2c, &spank_i2c_cfg)) < 0) {
	SPANK_EXTIO_LOG_RC_ERRNO(rc, "Unable to configure I2C");
	return rc;
    }
    if ((rc = spank_extio_set_report(SPANK_EXTIO_EVENT_NONE)) < 0) {
	SPANK_EXTIO_LOG_RC_ERRNO(rc, "Unable to configure report");
	return rc;
    }
    
    /*
     * Be on the safe side, de-assert data-ready interrupt
     */
    if ((rc = spank_extio_events_ready()) < 0) {
	SPANK_EXTIO_LOG_RC_ERRNO(rc, "Failed to de-assert data-ready interrupt");
    }

#if defined(SPANK_EXTIO_IRQ)
    /*
     * Enable GPIO pins (data-ready)
     */
    struct bitters_gpio_cfg spank_irq_cfg = {
        .dir       = BITTERS_GPIO_DIR_INPUT,
	.interrupt = BITTERS_GPIO_INTERRUPT_RISING_EDGE,
	.label     = "spank-int",
    };
    if ((rc = bitters_gpio_pin_enable(&spank_irq, &spank_irq_cfg)) < 0) {
	SPANK_EXTIO_LOG_RC_ERRNO(rc, "Unable to enable GPIO for IRQ processing");
	return rc;
    }
#endif
    
    /* Done 
     */
    SPANK_EXTIO_LOG("Initialized");
    return 0;
}


int
spank_extio_get_system_info(spank_extio_system_info_t *sysinfo)
{
    int rc;

    if (((rc = _spank_extio_i2c_read(SPANK_I2C_REG_VERSION,
                            sysinfo->version.application,
                            sizeof(sysinfo->version.application))) < 0) ||
	((rc = _spank_extio_i2c_read(SPANK_I2C_REG_SPANK_VERSION,
                            sysinfo->version.spank,
                            sizeof(sysinfo->version.spank))) < 0)) {
       return rc;
    }

    SPANK_EXTIO_LOG("Retrieved system information (app=<%s>, spank=<%s>)",
                   sysinfo->version.application,
                   sysinfo->version.spank);
    return 0;
}


int
spank_extio_start(int events_msk, int behaviour)
{
    int rc;
    
    if (((rc = spank_extio_set_report(events_msk)  ) < 0) ||
	((rc = spank_extio_set_behaviour(behaviour)) < 0)) {
	SPANK_EXTIO_LOG_RC_ERRNO(rc, "Failed to start");
	return rc;
    }

#if defined(SPANK_EXTIO_WAIT_SPINNING)
    spank_extio_events_msk = events_msk;
#endif

    SPANK_EXTIO_LOG("Started with report=<0x%02x> and behaviour=<0x%02x>",
		    events_msk, behaviour);
    return 0;
}


int
spank_extio_end()
{
    int rc = spank_extio_set_report(SPANK_EXTIO_EVENT_NONE);
    if (rc < 0) {
	SPANK_EXTIO_LOG_RC_ERRNO(rc, "Failed to end");
    } else {
	SPANK_EXTIO_LOG("Ended");
    }
    return rc;
}


int
spank_extio_wait(int timeout)
{
#if defined(SPANK_EXTIO_WAIT_SPINNING)
#if defined(SPANK_EXTIO_IRQ)
#warning using SPANK_EXTIO_WAIT_SPINNING is non-optimal, try wiring IRQ line
#endif
    uint32_t utimeout = timeout * 1000;
    while(1) {
	// Retrieve events
	int events = spank_extio_events_ready();
	if (events < 0) {
	    SPANK_EXTIO_LOG_RC_ERRNO(events, "Failed to retrieve events");
	    return events;
	}
	SPANK_EXTIO_LOG("Got events <0x%02x>", events);

	// Return if we got an event for which we are interested
	if (events & spank_extio_events_msk) {
	    return events;
	}

	// Deal with timeout (otherwise endless loop)
	if        (timeout == 0) {
	    SPANK_EXTIO_LOG("Returned without events");
	    return SPANK_EXTIO_EVENT_NONE;
	} else if (timeout >  0) {
	    if (usleep(SPANK_EXTIO_WAIT_SPINNING) < 0) {
		SPANK_EXTIO_LOG_ERRNO("Spin wait failed");
		return -errno;
	    }
	    if (utimeout < SPANK_EXTIO_WAIT_SPINNING ) {
		SPANK_EXTIO_LOG("Returned from timeout");
		return SPANK_EXTIO_EVENT_NONE;
	    }
	    utimeout -= SPANK_EXTIO_WAIT_SPINNING;
	}
    }
#elif defined(SPANK_EXTIO_IRQ)
    struct pollfd pollfd[1];
    int rc = bitters_gpio_irq_fill_poolfd(&spank_irq, &pollfd[0]);
    if (rc < 0) return rc;

    while(1) {
	// Wait for a poll-event (ie: an irq in our case)
	// return immediately in case of error (<0) or timeout (=0)
	// Note that timeout (=0) mapped to SPANK_EXTIO_EVENT_NONE (0x00)
	int n = poll(pollfd, __arraycount(pollfd), timeout);
	if (n < 0) {
	    if (errno == EINTR) {
		SPANK_EXTIO_LOG("Got interrupted by signal");
	    } else {
		SPANK_EXTIO_LOG_ERRNO("Unexpected return from polling");
	    }
	    SPANK_EXTIO_ASSERT(errno == EINTR);
	    return -errno;
	}
	if (n == 0) {
	    SPANK_EXTIO_LOG("Returned from timeout");
	    return 0;
	}

	if (pollfd[0].revents) {
	    // Acknowledge returned events (revents) to clear poll
	    if ((rc = bitters_gpio_irq_wait(&spank_irq)) < 0) {
		SPANK_EXTIO_LOG_RC_ERRNO(rc, "Failed to read IRQ event");
	    }

	    // Retrieve events ready
	    int events = spank_extio_events_ready();

	    // The case SPANK_EXTIO_EVENT_NONE is possible due
	    // to the "processing of interrupt in user space" where
	    // interrupt notification is delayed, it can also be due
	    // to multiple ExtIO flavors (I2C/USB) competing for the data
	    if (events == SPANK_EXTIO_EVENT_NONE) {
		// We return -EAGAIN to avoid it's interpretation
		// as timeout expired (as SPANK_EXTIO_EVENT_NONE is 0x00)
		// and to avoid to deal with processing of remaining timeout
		return -EAGAIN;
	    }

	    // Return events (or error)
	    if (events < 0) {
		SPANK_EXTIO_LOG_RC_ERRNO(events, "Failed to retrieve events");
	    } else {
		SPANK_EXTIO_LOG("Got events <0x%02x>", events);
	    }
	    return events;
	} 

	// We only watch for 1 poll-event, so this is unexpected
	SPANK_EXTIO_LOG("Wake-up from unexpected poll event"
			", keep waiting");
	SPANK_EXTIO_ASSERT(0);
    }
#else
#error "Compiling for IRQ, but the platform doesn't support it. Please define SPANK_EXTIO_WAIT_SPINNING, to use spin wait instead."
#endif
}


int
spank_extio_events_ready(void)
{
    uint8_t flags = 0;
    int     rc    = _spank_extio_i2c_read(SPANK_I2C_REG_DATA_READY,
					  &flags, sizeof(flags));
    return rc < 0 ? rc : flags;
}


int
spank_extio_fetch_distance_measurement(
	spank_extio_distance_measurement_t *dm,
	spank_extio_distance_measurement_extra_t *extra,
	spank_extio_info_t *info)
{
   
#if defined(__linux__) && (SPANK_EXTIO_PLATFORM == SPANK_EXTIO_PLATFORM_RPI)
    if (info != NULL) {
	SPANK_EXTIO_LOG("Linux BCM2835 used on RaspberryPI only support 1 read message");
	SPANK_EXTIO_LOG("spank_extio_fetch_distance_measurement() will fail with EOPNOTSUPP as info parameter has been requested");
    }
#endif

    if (extra != NULL) {
	SPANK_EXTIO_LOG("parameter extra in spank_extio_fetch_distance_measurement() is not supported for I2C");
	memset(extra, 0xff, sizeof(spank_extio_distance_measurement_extra_t));
    }
    
    uint8_t reg = SPANK_I2C_REG_DISTANCE_MEASUREMENT;
    const struct bitters_i2c_transfert xfr[] = {
	{ .buf = &reg, .len = sizeof(reg  ), .write = 1, },
        { .buf = dm,   .len = sizeof(*dm  ), .read  = 1, },
	{ .buf = info, .len = sizeof(*info), .read  = 1, },
    };
    int xfr_count = info ? 3 : 2;
    
    int rc = bitters_i2c_transfert(&spank_i2c, spank_i2c_addr, xfr, xfr_count);

    return rc < 0 ? rc : 0;
}


int
spank_extio_set_posture(spank_extio_posture_t *posture)
{
    return _spank_extio_i2c_write(SPANK_I2C_REG_POSTURE,
				  posture, sizeof(*posture));
}


int
spank_extio_get_posture(spank_extio_posture_t *posture)
{
    return _spank_extio_i2c_read(SPANK_I2C_REG_POSTURE,
				 posture, sizeof(*posture));
}


int
spank_extio_enable_frame_sniffer(void)
{
    return -EOPNOTSUPP;
}


int
spank_extio_disable_frame_sniffer(void)
{
    return -EOPNOTSUPP;
}


ssize_t
spank_extio_frame_sniffer(uint8_t *buf, size_t buflen) {
    return -EOPNOTSUPP;
}
